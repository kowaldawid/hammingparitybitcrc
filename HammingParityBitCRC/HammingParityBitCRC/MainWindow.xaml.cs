﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.HashFunction;
using System.Collections;

namespace HammingParityBitCRC
{
    public partial class MainWindow : Window
    {
        //Standard.CRC12_3GPP
        //Standard.CRC16_CCITTFALSE
        //Standard.CRC32
        private CRC crc;
                
        public byte[] MessageByte { get; set; }
        public string MessageAfterAll { get; set; }
        public byte[] MessageBinaryCRC { get; set; }
        public string MessageBinaryCRCParity { get; set; }
        public Binary MessageBinaryCRCParityHamming { get; set; }
        public Binary MessageAfteChecking { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        public byte[] BinaryStringToByteArray(string data)
        {
            List<Byte> byteList = new List<Byte>();

            for (int i = 0; i < data.Length; i += 8)
            {
                byteList.Add(Convert.ToByte(data.Substring(i, 8), 2));
            }
            return byteList.ToArray();
        }

        private string ByteArrayToString(byte[] array)
        {
            string binary = "";
            for (int i = 0; i < array.Length; i++)
            {
                binary += Convert.ToString(array[i], 2).PadLeft(8, '0');
            }
            return binary;
        }

        private void TableViewIndexBit(string text, int itemBitError = -1)
        {
            var tab = new Table();
            var gridLenghtConvertor = new GridLengthConverter();

            for (int i = text.Length; i >= 0; i--)
            {
                tab.Columns.Add(new TableColumn() { Name = "Col" + i.ToString(), Width = (GridLength)gridLenghtConvertor.ConvertFromString("10") });
            }

            tab.RowGroups.Add(new TableRowGroup());
            tab.RowGroups[0].Rows.Add(new TableRow());
            var tabRow = tab.RowGroups[0].Rows[0];
       
            tab.RowGroups[0].Rows.Add(new TableRow());
            var tabRow2 = tab.RowGroups[0].Rows[1];

            for (int i = text.Length; i > 0; i--)
            {
                var background = (i - 1 == itemBitError) ? Brushes.Red : Brushes.Transparent;
                tabRow.Cells.Add(new TableCell(new Paragraph(new Run(text[text.Length - i].ToString()))) { TextAlignment = TextAlignment.Center, Background = background });
                tabRow2.Cells.Add(new TableCell(new Paragraph(new Run((i - 1).ToString()))) { TextAlignment = TextAlignment.Center });
                tabRow.Cells[text.Length - i].BorderThickness = new Thickness(0.5, 0.5, 0.5, 0.5);
                tabRow.Cells[text.Length - i].BorderBrush = Brushes.White;
                tabRow2.Cells[text.Length - i].BorderThickness = new Thickness(0.5, 0.5, 0.5, 0.5);
                tabRow2.Cells[text.Length - i].BorderBrush = Brushes.Black;
            }

            console.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
            console.Document.PageWidth = text.Length * 13;
            console.Document.Blocks.Add(tab);
        }
        
        private void GetText_TextChanged(object sender, TextChangedEventArgs e)
        {
            buttonAfterAll.IsEnabled = false;
            buttonSetWhichBitError.IsEnabled = false;
            buttonHamming.IsEnabled = false;
            TextBox text = sender as TextBox;
            if (text != null)
            {
                MessageByte = Encoding.ASCII.GetBytes(text.Text);
            }

            FlowDocument myFlowDoc = new FlowDocument();
            myFlowDoc.Blocks.Add(new Paragraph(new Run(Environment.NewLine + "TEXT\r" + ByteArrayToString(MessageByte))));
            console.Document = myFlowDoc;
        }
    
        private void FinishText_LostFocus(object sender, RoutedEventArgs e)
        {
            if (crc != null)
                ConsoleViewCRC();
        }

        private void GetCRC_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            buttonAfterAll.IsEnabled = false;
            buttonSetWhichBitError.IsEnabled = false;
            buttonHamming.IsEnabled = false;
            ComboBoxItem crcWidth = (ComboBoxItem)getCRC.SelectedItem;
            CRC.Standard width;
            Enum.TryParse(crcWidth.Content.ToString(), out width);
          
            CRC.Setting argumentsCRC;
            CRC.Standards.TryGetValue(width, out argumentsCRC);

            //Bits-jest to szerokość słowa obliczeniowego algorytmu
            //Polynomial-ten parametr to po prostu nasz wielomian generujący
            //InitialValue-parametr, określający stan początkowy rejestru
            //ReflectIn,ReflectOut-parametr logiczny
            //XOROUT-wartoćci zwracanej przez procedurę jako końcowa wartość wyliczonej sumy
            argumentsCRC = new CRC.Setting(argumentsCRC.Bits, argumentsCRC.Polynomial, argumentsCRC.InitialValue, CRC.DefaultSettings.ReflectIn, CRC.DefaultSettings.ReflectOut, 0);
 
            crc = new CRC(argumentsCRC);
            if (MessageByte != null)
                ConsoleViewCRC();
        }

        private void ConsoleViewCRC()
        {
            buttonParityBit.IsEnabled = true;
            byte[] hashCRC = crc.ComputeHash(MessageByte);

            MessageBinaryCRC = new byte[MessageByte.Length + hashCRC.Length];
            MessageByte.CopyTo(MessageBinaryCRC, 0);
            hashCRC.CopyTo(MessageBinaryCRC, MessageByte.Length);
            console.AppendText(Environment.NewLine + "CRC" + crc.HashSize + "\r" + ByteArrayToString(MessageBinaryCRC));
        }

        private void AddParityBit(object sender, RoutedEventArgs e)
        {
            if (MessageByte != null)
            {
                buttonHamming.IsEnabled = true;
                int oneCounter = ByteArrayToString(MessageBinaryCRC).ToCharArray().Count(c => c == '1');
                char parityBit = ((oneCounter % 2 == 0) ? '0' : '1');
                MessageBinaryCRCParity = ByteArrayToString(MessageBinaryCRC);
                MessageBinaryCRCParity += parityBit;
                console.AppendText(Environment.NewLine + "Bit parzystosci\r" + MessageBinaryCRCParity);
            }
        }

        private void AddHammingCode(object sender, RoutedEventArgs e)
        {
            if (MessageBinaryCRCParity != null)
            {
                buttonAfterAll.IsEnabled = true;
                buttonSetWhichBitError.IsEnabled = true;
                Binary message = new Binary(MessageBinaryCRCParity.Select(c => c == '1' ? true : false));
                int columnsAmount = message.Count();
                int rowsAmount = (int)Math.Ceiling(Math.Log(columnsAmount + 1, 2)); //Uwaga
                BinaryMatrix H = Hamming.GenerateHMatrix(rowsAmount, columnsAmount);

                Binary verification = Hamming.GenerateVerificationBits(H, message);
                Binary frame = Binary.Concatenate(message, verification);
                MessageBinaryCRCParityHamming = frame;

                console.AppendText(Environment.NewLine + "Kod Hamminga");
                TableViewIndexBit(MessageBinaryCRCParityHamming.ToString());
            }
        }

        private void SetWhichBitError(object sender, RoutedEventArgs e)
        {
            if (MessageBinaryCRCParityHamming != null)
            {
                try
                {
                    int wchichBit = Convert.ToInt32(getWhichBitError.Text);
                    if (wchichBit >= 0 && wchichBit < MessageBinaryCRCParityHamming.Length)
                    {
                        MessageBinaryCRCParityHamming[MessageBinaryCRCParityHamming.Length - wchichBit - 1] = !MessageBinaryCRCParityHamming[MessageBinaryCRCParityHamming.Length - wchichBit - 1];
                        console.AppendText("Po przekłamaniu");
                        TableViewIndexBit(MessageBinaryCRCParityHamming.ToString(), wchichBit);
                    }
                }
                catch
                {
                    MessageBox.Show("No no, nie rób mi krzywdy :(", "", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void AfterAll(object sender, RoutedEventArgs e)
        {
            if (MessageBinaryCRCParityHamming != null)
            {
                int columnsAmount = MessageBinaryCRCParityHamming.Count();
                int rowsAmount = (int)Math.Ceiling(Math.Log(columnsAmount + 1, 2));
                columnsAmount = columnsAmount - rowsAmount;
                Binary receivedMessage = new Binary(MessageBinaryCRCParityHamming.Take(columnsAmount)); //Uwaga
                Binary receivedVerification = new Binary(MessageBinaryCRCParityHamming.Skip(columnsAmount));
                BinaryMatrix H = Hamming.GenerateHMatrix(rowsAmount, columnsAmount);
                Binary receivedMessageVerification = Hamming.GenerateVerificationBits(H, receivedMessage);
                Binary xor = receivedVerification ^ receivedMessageVerification;

                MessageAfteChecking = receivedMessage;
                if (xor.CountOnes() > 0)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("Odebrana wiadomość zawiera błąd podjąć próbę rekonstrukcji?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        BinaryMatrix HWithIdentity = Hamming.GenerateHWithIdentity(H);
                        int faultyBitPosition = Hamming.FindFaultyBit(HWithIdentity, xor);

                        Binary correctedFrame = new Binary(MessageBinaryCRCParityHamming.ToArray());
                        correctedFrame[faultyBitPosition] = !correctedFrame[faultyBitPosition];

                        Binary correctedFrameGeneratedVerify = Hamming.GenerateVerificationBits(H, new Binary(correctedFrame.Take(columnsAmount)));
                        Binary correctedFrameVerify = new Binary(correctedFrame.Skip(columnsAmount));
                        Binary correctionVerify = correctedFrameVerify ^ correctedFrameGeneratedVerify;

                        if (correctionVerify.CountOnes() == 0)
                        {
                            MessageAfteChecking = new Binary(correctedFrame.Take(columnsAmount));
                            MessageBox.Show("Udało się skorygować wiadomośći", "", MessageBoxButton.OK, MessageBoxImage.Information);
                            console.AppendText("Po rekonstrukcji");
                            TableViewIndexBit(correctedFrame.ToString(), correctedFrame.Length - faultyBitPosition - 1);
                        }
                        else
                            MessageBox.Show("Nie można skorygować wiadomośći", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                console.AppendText("Odebrana wiadomośći z zawartością CRC oraz bitem parzystości");
                console.AppendText(Environment.NewLine + MessageAfteChecking.ToString() + Environment.NewLine);
                CheckParityBit();
                CheckCRC();
            }
        }

        private void CheckParityBit()
        {
            int oneCounter = MessageAfteChecking.ToString().ToCharArray().Count(c => c == '1');
            if (oneCounter % 2 != 0)
                MessageBox.Show("Wiadomość wygląda na niepoprawną", "", MessageBoxButton.OK, MessageBoxImage.Error);
            MessageAfteChecking = new Binary(MessageAfteChecking.Take(MessageAfteChecking.Length - 1));
            console.AppendText("Odebrana wiadomośći z zawartością CRC");
            console.AppendText(Environment.NewLine + MessageAfteChecking.ToString() + Environment.NewLine);
        }


        private bool CheckIsCRCCorrect(byte[] crc)
        {
            foreach (byte c in crc)
            {
                if(c != 0)
                    return false;
            }
            return true;
        }

        private void CheckCRC()
        {
            byte[] checkHash = crc.ComputeHash(BinaryStringToByteArray(MessageAfteChecking.ToString()));

            if (!CheckIsCRCCorrect(checkHash))     
                MessageBox.Show("Wiadomość wygląda na niepoprawną błąd CRC", "", MessageBoxButton.OK, MessageBoxImage.Error);

            var lenght = crc.HashSize;

            if (crc.HashSize > 8 && crc.HashSize < 16)
            {
                lenght = 16;
            }

            MessageAfteChecking = new Binary(MessageAfteChecking.Take(MessageAfteChecking.Length - lenght));
            console.AppendText("Odebrana wiadomośći");
            console.AppendText(Environment.NewLine + MessageAfteChecking.ToString() + Environment.NewLine);
            console.AppendText("TEXT ODEBRANEJ WIADOMOŚCI:\r" + Encoding.ASCII.GetString(BinaryStringToByteArray(MessageAfteChecking.ToString())) + Environment.NewLine);
        }
    }
}
